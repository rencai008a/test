package com.example.motion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends Activity implements View.OnClickListener{
	Button menuButton;
	Button updateButton;
	TextView versiontv;
	String Version="";
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settingpage);
		menuButton = (Button) findViewById (R.id.menubt);
		menuButton.setOnClickListener(this);
		updateButton = (Button) findViewById (R.id.updatebt);
		updateButton.setOnClickListener(this);
		versiontv = (TextView) findViewById (R.id.version);
		//versiontv.setText(Version);
	}

	@Override
	public void onClick(View v) {
		Toast toast;
		switch(v.getId()){
		case R.id.menubt:
			Intent intent = new Intent();
			intent.setClass(SettingActivity.this,MenuActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_left,R.anim.out_to_right);
			break;
		case R.id.updatebt:
			toast = Toast.makeText(getApplicationContext(),"��δ�°汾�������ڴ���",Toast.LENGTH_SHORT);
			toast.show();
			break;
		}
	}
}
