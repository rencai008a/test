package com.example.motion;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Map;

import com.example.service.DBOpenHelper;
import com.example.service.PreferenceService;
import com.example.service.data;
import com.example.service.dataService;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener ,SensorEventListener {
	Button menuButton;
	Button startButton;
	TextView timetv;
	TextView goaltv;
	TextView statetv;
	TextView totaltv;
	TextView percentagetv;
	DecimalFormat format = new DecimalFormat("#.00");
	private SensorManager sensorManager;  /** 管理传感器 */
	private Sensor sensor ;
	private Button start1;    /** 控制是否注册传感器   */
	private Button exit;
	private Button pause; 
	private int n=0;                  /** x轴是递进的*/
	private int Hour=0,Minute=0,Second=0; /**   时间初始化*/
	private boolean start=false;      /** start控制绘图的开始与结束*/
	private float gravity[]=new float[] {0,0,0};  /** 中间数据 */
	private data userdata;         //用户数据，这是个业务类
    private dataService dataService;       //数据库服务
    private DBOpenHelper dbOpenHelper;     //第一次创建数据库
    private PreferenceService preferenceService;   //对每次的n储存
    private float exercise=0.0f;      //运动量
    private static final float u=0.014f; //你懂的
    private  int mg=70;       //体重
    private long mExitTime;
    private int every=2;                 //每次暂停保存的参数 
	private String time = "";
	private String FIRSTFLAG="1";
	private float total = 0;
	private int goal = 0;
	private String state = "不详";
	private int percentage = 100;
	private int BUTTONSTATE = 1;
	final int START = 1;
	final int PAUSE = 0;
	private int temp = 0;
	Handler handler =new Handler();  //更新时间
	Runnable task= new Runnable() {
		@Override
		public void run() {
			//判断时间，并进行加减
			if(Second<60)
			{
				Second+=1;
			}
			else if(Minute<60)
			{
				Minute+=1;
				Second=1;
			}
			else 
			{
				Hour+=1;
				Minute=1;
				Second=1;
			}
			
			//设置文本框为显示时间
			timeDisplay();
			//timetv.setText(Hour+":"+Minute+":"+Second);
			if(Second%10==0){
				calc(n);           //每10s计算一次运动量
				}
			//在run方法内部post或postDelayed方法
			total = exercise/1000;
			temp = (int)(goal - total);
			totaltv.setText(""+format.format(total)+"KJ");
			percentagetv.setText(""+temp+"KJ");
			handler.postDelayed(task, 1000);
			
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_homepage);
		find();
		menuButton.setOnClickListener(this);
		startButton.setOnClickListener(this);
	}
    public void find() {
    	preferenceService= new PreferenceService(this);
        dataService=new dataService(getApplicationContext());
        menuButton = (Button) findViewById (R.id.menubt);
        startButton = (Button) findViewById (R.id.startbt);
        timetv = (TextView) findViewById (R.id.time);
		goaltv = (TextView) findViewById (R.id.goaltext);
		Map<String,String>paramss=preferenceService.Personpreferences();
		goal = Integer.valueOf(paramss.get("goal"));
		goaltv.setText(""+goal+"KJ");
		statetv = (TextView) findViewById (R.id.state);
		totaltv = (TextView) findViewById (R.id.totalenergy);
		percentagetv = (TextView) findViewById (R.id.percentage);
		percentagetv.setText(""+goal+"KJ");
		sensorManager=(SensorManager)this.getSystemService(SENSOR_SERVICE);      //传感器管理
        sensor=sensorManager.getDefaultSensor(sensorManager.SENSOR_ACCELEROMETER);//实例化加速度传感器
        Map<String,String>paramsss=preferenceService.Ifpreferences();
        //int HMS=Integer.parseInt(paramsss.get("cur_time"));
        Calendar calendar = Calendar.getInstance();
        String date = ""+calendar.get(Calendar.YEAR)+ (calendar.get(Calendar.MONTH)+1)+ calendar.get(Calendar.DAY_OF_MONTH);
        Map<String,String>paramssss=preferenceService.Daypreferences();
        if(date!=paramssss.get("Daytime")) {
        	n=0;
			preferenceService.save(n);
			dataService.delete();
        }
        preferenceService.save(date);
        //Hour=HMS/3600;
        //Minute=HMS/60-Hour*60;
        //Second=HMS-Hour*3600-Minute*60;
        timeDisplay();
        if(paramsss.get("IFFIRST")=="1"){
        	Toast.makeText(getApplicationContext(), "首次登录，请设置信息",Toast.LENGTH_SHORT ).show();
        }else {
        	FIRSTFLAG="2";
        }
        total=Float.parseFloat(preferenceService.totalpreferences().get("total"));
    }
    /**
     * 
     * @param 时间显示
     */
    public void timeDisplay(){
    	if(Second<=9&&Minute<=9&&Hour<=9){
			timetv.setText("0"+Hour+":"+"0"+Minute+":"+"0"+Second);
		}
		if(Second>9&&Minute<=9&&Hour<=9){
			timetv.setText("0"+Hour+":"+"0"+Minute+":"+Second);
		}
		if(Second<=9&&Minute>9&&Hour<=9){
			timetv.setText("0"+Hour+":"+Minute+":"+"0"+Second);
		}
		if(Second>9&&Minute>9&&Hour<=9){
			timetv.setText("0"+Hour+":"+Minute+":"+"0"+Second);
		}
		if(Second<=9&&Minute<=9&&Hour>9){
			timetv.setText(Hour+":"+"0"+Minute+":"+"0"+Second);
		}
		if(Second>9&&Minute<=9&&Hour>9){
			timetv.setText(Hour+":"+"0"+Minute+":"+Second);
		}
		if(Second<=9&&Minute>9&&Hour>9){
			timetv.setText(Hour+":"+Minute+":"+"0"+Second);
		}
		if(Second>9&&Minute>9&&Hour>9){
			timetv.setText(Hour+":"+Minute+":"+Second);
		}
    }
  //计算运动量
  	 public void calc(int num) {             //num=n   n为记录数
  		/** 我的思路是这样的，首先time1为第二条记录的时间减去第一条记录的时间
  		 *  然后从第二条记录开始，一直到倒数第二条记录，数据量一大，少去的记录可忽略
  		 *  然后t=1/2 t1方-t2方
  		 *  最后求和   同时求出平均能耗，再判断运动状态
  		 * */
  		int i=every;
  		float pausesum=0;
  		try{
  		for( i=every;i<num;i++){            
  		 long time1=dataService.find(i).getTime()-dataService.find(i-1).getTime();
  		 long time2=dataService.find(i-1).getTime()-dataService.find(i-2).getTime();
  		 float t=(float) ((time2+time1)*(time2-time1)/1000.0);
  		 float gm=dataService.find(i).getGm()-dataService.find(i-1).getGm();
  		 pausesum =(float) Math.abs(0.25*u*gm*t*mg);
  		 if(pausesum<500){
  		 exercise+=pausesum;
  		 }
  		}
  		}catch(Exception e){
  			e.printStackTrace();
  		}
  		every=i-1;
  		if(pausesum<=0.5){
  			    statetv.setText("站立或静坐" );
  		}
  			else if(0.5<pausesum&&pausesum<=4)
  				statetv.setText("步行" );
  			else if(4<pausesum&&pausesum<=6)
  				statetv.setText("上楼" );
  			else if(6<pausesum&&pausesum<=9)
  				statetv.setText("下楼" );
  			else if(9<pausesum&&pausesum<=16)
  				statetv.setText("跑步" );
  			else if(16<pausesum&&pausesum<=25)
  				statetv.setText("上下跳动" );
  			else 
  				statetv.setText("未知" );
  	}
  	public void show(boolean b) {         //显示textview5是否在屏幕上打印  也是由start控制 
		if(b==false)
			totaltv.setText(""+exercise);
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch(item.getItemId()) {
		case R.id.action_settings:
			Intent intent =new Intent();
			intent.setClass(this, Setting.class);
			startActivity(intent);
			this.finish();
            break;
		}
		return true;
	}
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);     /**解除注册，一定要及时解除，费电很快，Google里说的*/
	}
    protected void onResume() {
		super.onResume();
		sensorManager.registerListener(this, 
				sensor, sensorManager.SENSOR_DELAY_NORMAL); /**注册传感器*/
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.menubt:
			preferenceService.save(FIRSTFLAG, Hour*3600+Minute*60+Second);
			preferenceService.savetotal((int)total);
			if(BUTTONSTATE == START){
				BUTTONSTATE = PAUSE;
				start=true;
				Map<String,String>params=preferenceService.preferences(); //这是保存的偏好参数，当手动点击结束按钮，n的值会被保存，再次点击开始后，n会被重新提取
				n=Integer.parseInt(params.get("ndata"));
				Map<String,String>param=preferenceService.Personpreferences();
				mg=Integer.parseInt(param.get("weight"));
				handler.post(task);
				startButton.setBackgroundResource(R.drawable.btpause);
			}
			Intent intent = new Intent();
			intent.setClass(MainActivity.this,MenuActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_left,R.anim.out_to_right); 
			break;
		case R.id.startbt:
			if(BUTTONSTATE == START){
				BUTTONSTATE = PAUSE;
				start=true;
				Map<String,String>params=preferenceService.preferences(); //这是保存的偏好参数，当手动点击结束按钮，n的值会被保存，再次点击开始后，n会被重新提取
				n=Integer.parseInt(params.get("ndata"));
				Map<String,String>param=preferenceService.Personpreferences();
				mg=Integer.parseInt(param.get("weight"));
				handler.post(task);
				startButton.setBackgroundResource(R.drawable.btpause);
			}else{
				BUTTONSTATE =START;
				start=false;             //停止制图
				preferenceService.save(n); //保存偏好参数
				//show(start);               //显示运动量
				//dataService.save(System.currentTimeMillis(), exercise);
				handler.removeCallbacks(task);
				startButton.setBackgroundResource(R.drawable.btstart);
			}
			break;
			
		}
		
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {     //传感器精度发生改变时调用
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {        // 加速度发生改变时会调用
		final float alpha = 0.8f;
        float linear_acceleration[]=new float[3];
        
        double gm;
        /*接收三个加速度,用低通滤波器分离出重力加速度*/
		gravity[0]  = alpha * gravity[0] + (1 - alpha) * event.values[0];
		gravity[1]  = alpha * gravity[1] + (1 - alpha) * event.values[1];
		gravity[2]  = alpha * gravity[2] + (1 - alpha) * event.values[2];
		/** 用高通滤波法剔除重力干扰*/
		linear_acceleration[0] = event.values[0] - gravity[0];
		linear_acceleration[1] = event.values[1] - gravity[1];
		linear_acceleration[2] = event.values[2] - gravity[2];
		gm=Math.sqrt(((linear_acceleration[0]*linear_acceleration[0])+(linear_acceleration[1]*linear_acceleration[1])+(linear_acceleration[2]*linear_acceleration[2]))/3);
		if(start)
		{
			userdata=new data(n,(float)gm,System.currentTimeMillis());//数据库保存每一次的数值
			dataService.save(userdata);
			n++;

		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {   //双击back退出
	     if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){  
	         if ((System.currentTimeMillis() - mExitTime) > 2000 ) {  
	               
	             Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();  
	             mExitTime = System.currentTimeMillis();  
	         }else{  
	        	 n=0;
	 			 preferenceService.save(n);
	 			 dataService.delete();
	             SysApplication.getInstance().exit();  
	         }  
	           
	         return true;  
	     }  
	      return super.onKeyDown(keyCode, event); 
    }
}
