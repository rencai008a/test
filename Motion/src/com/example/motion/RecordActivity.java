package com.example.motion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RecordActivity extends Activity implements View.OnClickListener{
	Button menuButton;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recordpage);
		menuButton = (Button) findViewById (R.id.menubt);
		menuButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.menubt:
			Intent intent = new Intent();
			intent.setClass(RecordActivity.this,MenuActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_left,R.anim.out_to_right);
			break;
		}
	}
}
