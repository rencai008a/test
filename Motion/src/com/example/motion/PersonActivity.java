package com.example.motion;

import java.util.Map;

import com.example.service.PreferenceService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PersonActivity extends Activity implements View.OnClickListener{
	Button editButton;
	TextView userNametv;
	TextView sexINFtv;
	TextView heightINFtv;
	TextView weightINFtv;
	TextView bestvalueINFtv;
	//TextView continuousINFtv;
	Button menuButton;
	private PreferenceService preferenceService;
	private String Username;
	private int Sex = 1;
	private int Height = 0;
	private int Weight = 0;
	private int goal = 0;
	//private int days = 0;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personpage);
		preferenceService= new PreferenceService(this);
		editButton = (Button) findViewById (R.id.editbt);
		editButton.setOnClickListener(this);
		sexINFtv = (TextView) findViewById (R.id.sexINF);
		heightINFtv = (TextView) findViewById (R.id.heightINF);
		weightINFtv = (TextView) findViewById (R.id.weightINF);
		bestvalueINFtv = (TextView) findViewById (R.id.bestvalueINF);
		//continuousINFtv = (TextView) findViewById (R.id.continuousINF);
		userNametv = (TextView) findViewById (R.id.username);
		menuButton = (Button) findViewById (R.id.menubt);
		menuButton.setOnClickListener(this);
		Map<String,String> params= preferenceService.Personpreferences();
		Sex = Integer.valueOf(params.get("sex"));
		Username = params.get("name");
		Height = Integer.valueOf(params.get("height"));
		Weight = Integer.valueOf(params.get("weight"));
		
		if(Sex==1){
			sexINFtv.setText("男");
		}
		else
		{
			sexINFtv.setText("女");
		}
		if(!Username.equals("")){
			userNametv.setText("用户名："+Username);
		}else{
			userNametv.setText("用户名");
		}
		bestvalueINFtv.setText(params.get("goal")+"KJ");
		heightINFtv.setText(Height+"cm");
		weightINFtv.setText(Weight+"KG");
		//continuousINFtv.setText(days+"Days");
		
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		switch(v.getId()){
		case R.id.editbt:
			intent.setClass(PersonActivity.this,PersonEditActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.zoomin,R.anim.zoomout);
			break;
		case R.id.menubt:
			intent.setClass(PersonActivity.this,MenuActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_left,R.anim.out_to_right);
			break;
		}
		
	}
}
