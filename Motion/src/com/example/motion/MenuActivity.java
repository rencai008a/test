package com.example.motion;


import java.util.Map;

import com.example.service.PreferenceService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends Activity implements View.OnClickListener{
	Button homeButton;
	Button personButton;
	Button recordButton;
	Button settingButton;
	String UserName;
	String Version;
	TextView versiontv;
	TextView UserNametv;
	private PreferenceService preferenceService;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menupage);
		preferenceService= new PreferenceService(this);
		homeButton = (Button) findViewById (R.id.menuhomebt);
		homeButton.setOnClickListener(this);
		personButton = (Button) findViewById (R.id.menupersonbt);
		personButton.setOnClickListener(this);
		//recordButton = (Button) findViewById (R.id.menurecordbt);
		//recordButton.setOnClickListener(this);
		settingButton = (Button) findViewById (R.id.menusettingbt);
		settingButton.setOnClickListener(this);
		versiontv = (TextView) findViewById (R.id.menuversion);
		UserNametv = (TextView) findViewById (R.id.username);
		Map<String,String>person= preferenceService.Personpreferences();
		UserName=person.get("name");
		if(UserName.equals("")){
			UserNametv.setText("用户名");
		}else{
			UserNametv.setText("用户名："+UserName);
		}
		
		//versiontv.setText(Version);
	}
	
	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		switch(v.getId()){
		case R.id.menuhomebt:
			intent.setClass(MenuActivity.this,MainActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left); 
			break;
		case R.id.menupersonbt:
			intent.setClass(MenuActivity.this,PersonActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
			break;
		/*case R.id.menurecordbt:
			intent.setClass(MenuActivity.this,RecordActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
			break;*/
		case R.id.menusettingbt:
			intent.setClass(MenuActivity.this,SettingActivity.class);
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
			break;
		}
	}
}
