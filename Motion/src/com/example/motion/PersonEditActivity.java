package com.example.motion;

import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.service.PreferenceService;

public class PersonEditActivity extends Activity implements View.OnClickListener{
	Button completeButton;
	Button cancleButton;
	EditText nameET;
	EditText sexET;
	EditText heightET;
	EditText weightET;
	private PreferenceService preferenceService;
	private String UserName;
	private int Sex = 1;
	private int Height = 0;
	private int Weight = 0;
	private int total=0;
	private String UserNameET = "";
	private String HeightET = "";
	private String WeightET = "";
	private String SexET = "";
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personalsetting);
		preferenceService= new PreferenceService(this);
		completeButton = (Button) findViewById (R.id.completebt);
		completeButton.setOnClickListener(this);
		cancleButton = (Button) findViewById (R.id.canclebt);
		cancleButton.setOnClickListener(this);
		nameET = (EditText) findViewById (R.id.nameet);
		sexET = (EditText) findViewById (R.id.sexet);
		heightET = (EditText) findViewById (R.id.heightet);
		weightET = (EditText) findViewById (R.id.weightet);
		Map<String,String>person= preferenceService.Personpreferences();
		UserName = person.get("name");
		Height = Integer.valueOf(person.get("height"));
		Weight = Integer.valueOf(person.get("weight"));
		Sex=Integer.valueOf(person.get("sex"));
	}
	public void receipt(){
		UserNameET = nameET.getText().toString();
		SexET = sexET.getText().toString();
		HeightET = heightET.getText().toString();
		WeightET = weightET.getText().toString();
	}
	public boolean check(){
		boolean FLAG = true;
		boolean state1 = true;
		boolean state2 = true;
		boolean state3 = true;
		boolean state4 = true;
		Toast toast;
		String error="错误：";
		String str="信息输入成功.\n";
		if(!UserNameET.equals("")){
			if(UserNameET.length()>20){
				state1 = false;
			}
			else{
				UserName = UserNameET;
				str+=("用户名："+UserName+"\n");
			}
		}
		if(!SexET.equals("")){
			if(SexET.equals("1")||SexET.equals("0")){
				Sex = Integer.valueOf(SexET);
				if(Sex == 1){
					str+=("性别：男\n");
				}else{
					str+=("性别：女\n");
				}
			}
			else{
				state2 = false;
			}
		}
		if(!HeightET.equals("")){
			if(Integer.valueOf(HeightET) >200 && Integer.valueOf(HeightET) <100){
				state3 = false;
			}
			else{
				Height = Integer.valueOf(HeightET);
				str+=("身高："+Height+"CM\n");
			}
		}
		if(!WeightET.equals("")){
			if(Integer.valueOf(WeightET)>200 && Integer.valueOf(WeightET)<30){
				state4 = false;
			}
			else{
				Weight = Integer.valueOf(WeightET);
				str+=("体重："+Weight+"KG\n");
			}
		}
		if(state1==false){
			error+="输入用户名过长";
			FLAG = false;
		}
		if(state2==false||state3==false||state4==false){
			if(FLAG==false){
				error+="且输入数字不符合";
			}else{
				error+="输入数字不符合";
				FLAG = false;
			}
		}
		
		if(FLAG == true){
			if(!str.equals("信息输入成功.\n")){
				toast = Toast.makeText(getApplicationContext(),str,Toast.LENGTH_SHORT);
				toast.show();
			}else{
				toast = Toast.makeText(getApplicationContext(),"信息未经任何更改!",Toast.LENGTH_SHORT);
				toast.show();
			}
		}else{
			toast = Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT);
    		toast.show();
		}
		return FLAG;
	}
	public void CalcTotal () {
		if(Sex == 1){
			total = (int)((double)((Weight-30)/170.0)*840+9250);
		}else{
			total = (int)((double)((Weight-30)/170.0)*840+8820);
		}
	}
	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		intent.setClass(PersonEditActivity.this, PersonActivity.class);
		switch(v.getId()){
		case R.id.completebt:
			receipt();
			if(check()){
				CalcTotal();
				Map<String,String>params=preferenceService.Ifpreferences();
				preferenceService.save(UserName, Integer.valueOf(Sex), Integer.valueOf(Height), Integer.valueOf(Weight),total);
				preferenceService.save("2", Integer.parseInt(params.get("cur_time")));
				this.startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.o_zoomin,R.anim.o_zoomout);
			}
			break;
		case R.id.canclebt:
			this.startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.o_zoomin,R.anim.o_zoomout);
			break;
		}
		
	}
}
