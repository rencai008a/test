package com.example.motion;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class StartActivity extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		new Handler().postDelayed(new Runnable()
		{

			@Override
			public void run()
			{
				Intent intent=new Intent(StartActivity.this,MainActivity.class);
				StartActivity.this.startActivity(intent);
				StartActivity.this.finish();
				overridePendingTransition(R.anim.zoomin,R.anim.zoomout);
			}
		}, 3000);
	}
}
