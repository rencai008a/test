package com.example.service;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.service.data;
public class dataService {
	private DBOpenHelper dbOpenHelper;
    public dataService(Context context) {
		this.dbOpenHelper = new DBOpenHelper(context);
	}
	public void save(data d){
		SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
		db.execSQL("insert into data(id,gm,time) values(?,?,?)", 
				new Object[]{d.getId(),d.getGm(),d.getTime()});
    }
	public void save(long time,float ex){
		SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
		db.execSQL("insert into PersonData(time,data) values(?,?)", 
				new Object[]{time,ex});
    }
    public data find(Integer id){
    	SQLiteDatabase db=dbOpenHelper.getReadableDatabase();
    	Cursor cursor=db.rawQuery("select * from data where id=?",new String []{ id.toString()});
    	if(cursor.moveToFirst()){
    		int i=cursor.getInt(cursor.getColumnIndex("id"));
    		float g=cursor.getFloat(cursor.getColumnIndex("gm"));
    		long time=cursor.getLong(cursor.getColumnIndex("time"));
    		cursor.close();
    		return new data(i,g,time);
    	}
    	cursor.close();
    	return null;
  } 
    public long  getCount(){
    	SQLiteDatabase db=dbOpenHelper.getReadableDatabase();
    	Cursor cursor=db.rawQuery("select count(*) from data", null);
    	cursor.moveToFirst();
    	long result=cursor.getLong(0);
    	cursor.close();
    	return result;
    }
    public void delete(){
    	SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
    	db.execSQL("delete from data");
    }
}
