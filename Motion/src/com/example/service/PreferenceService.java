package com.example.service;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.Preference;
//ƫ�ò�������
public class PreferenceService {
 private Context context;
 
	public PreferenceService(Context context) {
	this.context = context;
}   
	public void save(String Daytime) {
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		Editor editor =preferences.edit();
		editor.putString("Daytime", Daytime);
		editor.commit();
	}
	public void save(String first,int cur_time) {
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		Editor editor =preferences.edit();
		editor.putString("IFFIRST", first);
		editor.putInt("cur_time", cur_time);
		editor.commit();
	}
	public void save(int n) {
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		Editor editor =preferences.edit();
		editor.putInt("ndata", n);
		editor.commit();
	}
	public void savetotal(int n) {
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		Editor editor =preferences.edit();
		editor.putInt("total", n);
		editor.commit();
	}
	public void save(String name,int sex,int height,int weight,int goal) {
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		Editor editor =preferences.edit();
		editor.putString("name", name);
		editor.putInt("sex", sex);
		editor.putInt("height", height);
		editor.putInt("weight", weight);
		editor.putInt("goal", goal);
		editor.commit();
	}

	public Map<String ,String>preferences(){
		Map<String,String>params= new HashMap<String, String>();
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		params.put("ndata", String.valueOf(preferences.getInt("ndata", 0)));
		return params;
	}
	public Map<String ,String>totalpreferences(){
		Map<String,String>params= new HashMap<String, String>();
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		params.put("total", String.valueOf(preferences.getInt("total", 0)));
		return params;
	}
	public Map<String ,String>Personpreferences(){
		Map<String,String>params= new HashMap<String, String>();
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		params.put("name", preferences.getString("name", ""));
		params.put("sex", String.valueOf(preferences.getInt("sex", 1)));
		params.put("height", String.valueOf(preferences.getInt("height", 0)));
		params.put("weight", String.valueOf(preferences.getInt("weight", 0)));
		params.put("goal", String.valueOf(preferences.getInt("goal", 0)));
		return params;
	}
	public Map<String ,String>Ifpreferences(){
		Map<String,String>params= new HashMap<String, String>();
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		params.put("IFFIRST", preferences.getString("IFFIRST", " "));
		params.put("cur_time", String.valueOf(preferences.getInt("cur_time", 0)));
		return params;
	}
	public Map<String ,String>Daypreferences(){
		Map<String,String>params= new HashMap<String, String>();
		SharedPreferences preferences= context.getSharedPreferences("data", Context.MODE_PRIVATE);
		params.put("Daytime", preferences.getString("Daytime", " "));
		return params;
	}
}
