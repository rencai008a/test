package com.example.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {

	public DBOpenHelper(Context context) {
		super(context,"database.db", null, 2); 
	}

	

	@Override
	public void onCreate(SQLiteDatabase arg0) { //数据库第一次被创建的时候
		arg0.execSQL("create table data(id integer primary Key autoincrement,gm real,time integer)");
		arg0.execSQL("create table PersonData(time integer primary key autoincrement,data real)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		arg0.execSQL("create table PersonData(time integer primary key autoincrement,data real)");

	}

}
