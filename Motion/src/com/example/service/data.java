package com.example.service;

public class data {
  Integer id;
  float gm;
  long time;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public float getGm() {
	return gm;
}
public void setGm(float gm) {
	this.gm = gm;
}
public long getTime() {
	return time;
}
public void setTime(long time) {
	this.time = time;
}
public data(){}
public data(Integer id, float gm, long time) {
	this.id = id;
	this.gm = gm;
	this.time = time;
}
}
